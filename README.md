scout-rfp-demo
==============
Loads giphys after powering it on with the press of the giant button.
Doesn't try fetching or alter redux state before it's been turned on.

Has tests for components, reducers, component + redux integration,
and the data fetcher.

You can trigger a failure in fetching by setting, e.g. `window.alt_url = '123'`.

Specification
-------------
Create application using data from `http://api.giphy.com/v1/gifs/trending?api_key=…`

Requirements:
* Initially there should be a button. Clicking a button starts data fetching.
* "Loading" message/UI component should be shown while data is fetched
* Error modal should be shown when data fetch fails
* Application parts should be tested
* React framework
* Application state management using Redux or GraphQL client-side framework
* Pagination and styling is not necessary, unordered list of images will be good enough
* Code should be pushed to remote GIT repository for review
