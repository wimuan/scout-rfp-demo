import { connect } from 'react-redux'
import React, { Component } from 'react'
// import classNames from 'classnames'
import { fetchGiphys } from './actions'
import './Inflist.css'

export class Inflist extends Component {
  render() {
    return (
      <div className="Inflist" ref={el => this.root = el}>
        {this.props.items.map((item, index) =>
          <div key={index} className="item">
            {
              (item.embed_url) ? (
                <iframe title={item.title} src={item.embed_url} />
              ) : (
                `item: ${item}`
              )
            }
          </div>
        )}
      </div>
    )
  }

  shouldLoadMoreContent() {
    const docHeight = document.documentElement.getBoundingClientRect().height
    return this.root.offsetHeight < docHeight * 2
  }

  componentDidMount() {
    if (this.shouldLoadMoreContent()) {
      this.props.fetchGiphys()
    }
  }

  componentDidUpdate(...args) {
    return this.componentDidMount.apply(this, ...args)
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = {
  fetchGiphys
}

export default connect(mapStateToProps, mapDispatchToProps)(Inflist)
