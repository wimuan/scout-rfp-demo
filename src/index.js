import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import { createLogger } from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import { from } from 'rxjs'
import { filter, first } from 'rxjs/operators'

import registerServiceWorker from './registerServiceWorker'

import './index.css'
import App from './App'
import rootReducer from './reducers'
import { loadMoreOnBottom } from './actions'

const loggerMiddleware = createLogger({
  duration: true,
  timestamp: false,
  collapsed: true,
})

const store = createStore(
  rootReducer,
  {items: []},
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
  )
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'))

store.dispatch(loadMoreOnBottom())

from(store).pipe(
  filter(state => state.failed),
  first(),
).subscribe(() => alert('fetching failed'))

Object.assign(window, {store, dispatch: store.dispatch})

registerServiceWorker()
