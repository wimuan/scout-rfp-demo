import _ from 'lodash'
import { createAction } from 'redux-actions'
import 'node-fetch'
import promiseFinally from 'promise.prototype.finally'

export const powerOn  = createAction('POWER_ON')
export const itemAdd  = createAction('ITEM_ADD')
export const fetching = createAction('FETCHING')
export const failed   = createAction('FAILED')

export const loadMoreOnBottom = action => async dispatch => {
  // https://stackoverflow.com/a/33754031 : couldn't find on npm
  function getScrollXY(){var a=0,b=0;return"number"==typeof window.pageYOffset?(b=window.pageYOffset,a=window.pageXOffset):document.body&&(document.body.scrollLeft||document.body.scrollTop)?(b=document.body.scrollTop,a=document.body.scrollLeft):document.documentElement&&(document.documentElement.scrollLeft||document.documentElement.scrollTop)&&(b=document.documentElement.scrollTop,a=document.documentElement.scrollLeft),[a,b]} // eslint-disable-line
  function getDocHeight(){var a=document;return Math.max(a.body.scrollHeight,a.documentElement.scrollHeight,a.body.offsetHeight,a.documentElement.offsetHeight,a.body.clientHeight,a.documentElement.clientHeight)}

  document.addEventListener('scroll', _.throttle(function() {
    if (getDocHeight() - window.innerHeight * 2 < getScrollXY()[1]) {
      dispatch(fetchGiphys())
    }
  }, 100))
}

export const API_BASE_URL = 'https://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC'

// synchronous that could return a promise and would be easier testable
export const giphyFetcher = (dispatch, getState) => {
  const state = getState()
  const offset = (state.items || []).length

  /* I'm not so sure there couldn't be a race condition here, if giphyFetcher's
   * called twice fast enough. No idea what js guarantees here */
  if (offset > 100 || !state.power || state.fetching) {
    return
  }

  const base = window.alt_url || (module.exports && module.exports.API_BASE_URL) || API_BASE_URL
  const fail = err => { console.error('couldn\'t fetch', err); dispatch(failed()) }

  dispatch(fetching(true))

  return promiseFinally(fetch(`${base}&limit=3&offset=${offset}`)
      .then(async res => {
        if (res.status !== 200) fail(res)
        const json = await res.json()
        json.data.forEach(item => dispatch(itemAdd(item)))
      })
      .catch(fail))
    .then(() => dispatch(fetching(false)))
}

export const fetchGiphys = () => async (dispatch, getState) => {
  const items = getState().items || []
  if (items.length === 0) {
    // wait for the button animation, loader icon
    await new Promise(r => setTimeout(r, 1500))
  }

  giphyFetcher(dispatch, getState)
}
