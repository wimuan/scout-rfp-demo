import _ from 'lodash/fp'
import { connect } from 'react-redux'
import classNames from 'classnames'
import React, { Component } from 'react'

import './App.css'
import { fetchGiphys, powerOn } from './actions'
import Inflist from './Inflist'

export class App extends Component {
  render() {
    const loading_or_content = this.props.items && this.props.items.length !== 0 ? (
      <Inflist items={this.props.items} />
    ) : (
      <img src={require('./images/loading.gif')} alt="loading" className="loading" />
    )

    const startUp = () => {
      this.props.powerOn()
      this.props.fetchGiphys()
    }

    const classes = {
      App: true,
      on: this.props.power,
      failed: this.props.failed
    }

    return (
      <div className={classNames(classes)}>
        <a className="power" onClick={() => startUp()}>
          <span>download fresh giphys</span>
        </a>
        {this.props.power && !this.props.failed && loading_or_content}
      </div>
    )
  }
}

const mapStateToProps = _.pick(['power', 'items', 'failed'])

const mapDispatchToProps = {
  powerOn,
  fetchGiphys
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
