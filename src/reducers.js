import { powerOn } from './actions'
import fp from 'lodash/fp'

import { handleActions } from 'redux-actions'
import { handleActions as handleActionsFp } from 'redux-fp'

// inlined from https://github.com/redux-utilities/reduce-reducers/blob/b4e3cc757c00ecb1d45995e16fa61c674d94aba9/src/index.js
const reduceReducers = (...reducers) => (prevState, value, ...args) =>
  reducers.reduce(
    (newState, reducer) => reducer(newState, value, ...args),
    prevState
  );

const actionsReducer = handleActions({
  [powerOn]: (state) => Object.assign({}, state, {power: true}),
}, {})

const fpReducer = handleActionsFp({
  ITEM_ADD: ({payload}) => state => {
    if (state && state.power && !state.failed) {
      return fp.update('items', items => [...(items || []), payload])(state)
    } else {
      return state
    }
  },

  FAILED: ({payload}) => fp.update('failed', () => true),
  FETCHING: ({payload}) => fp.update('fetching', () => payload),
})

const rootReducer = reduceReducers(
  actionsReducer,
  (s, a) => fpReducer(a)(s),
)

export default rootReducer
