import 'chai/register-should'
import { createStore } from 'redux'
import rootReducer from '../src/reducers'
import { itemAdd } from '../src/actions'

describe('reducers', function () {
  let store

  subject(function () {
    return {}
  })

  beforeEach(function () {
    store = createStore(rootReducer, $subject)
  })

  it('shouldn\'t add items when it\'s off', function () {
    should.not.exist(store.getState().items)
    store.dispatch(itemAdd(1))
    should.not.exist(store.getState().items)
  })

  describe('with "powered on" state', function () {
    subject(function () {
      return {power: true}
    })

    it('shouldn\'t add items when it\'s on', function () {
      should.not.exist(store.getState().items)
      store.dispatch(itemAdd(1))
      should.exist(store.getState().items)
    })
  })
})
