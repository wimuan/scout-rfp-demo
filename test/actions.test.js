import { expect } from 'chai'
import configureMockStore from 'redux-mock-store'
import fetchMock from 'fetch-mock'

import { API_BASE_URL, giphyFetcher, itemAdd, fetching } from '../src/actions'
import * as Actions from '../src/actions'
import { createSandbox, fake } from 'sinon'

describe('actions', function () {
  let sinon

  const mockStore = configureMockStore()

  beforeEach(() => {
    const sinon = createSandbox()
    sinon.replace(Actions, 'API_BASE_URL', 'https://0.0.0.0/')

    fetchMock.restore()
    fetchMock.get('https://0.0.0.0/&limit=3&offset=0', { data: [23] })
    fetchMock.get('https://0.0.0.0/&limit=3&offset=1', { data: [23] })
  })

  it('does nothing if it\'s turned off', async function () {
    const store = mockStore({})

    await giphyFetcher(store.dispatch, store.getState)
    expect(store.getActions()).to.deep.equal([])
  })

  it('calls fetch', async function () {
    const store = mockStore({power: true})

    await giphyFetcher(store.dispatch, store.getState)
    expect(store.getActions()).to.deep.equal([
      fetching(true),
      itemAdd(23),
      fetching(false),
    ])
  })
})
