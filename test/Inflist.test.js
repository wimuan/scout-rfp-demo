import React from 'react'
import { mount } from 'enzyme'
import { expect } from 'chai'

import { Inflist } from '../src/Inflist'

describe('(Component) Inflist', function () {
  let wrapper, sinon

  def('state', () => ({
    items: ["test", {embed_url: 'embed_url', title: 'title'}]
  }))

  beforeEach(() => {
    wrapper = mount(<Inflist items={$state.items} fetchGiphys={() => {}} />)
  })

  it('renders', () => {
    expect(wrapper).to.have.length(1)
  })

  it('should render two items', () => {
    expect(wrapper.find('.item').length).to.deep.equal(2)
  })

  it('should render an iframe', () => {
    expect(wrapper.find('iframe').props()).to.have.property('src', 'embed_url')
  })
})
