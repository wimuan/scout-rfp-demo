import React from 'react'
import { mount } from 'enzyme'
import { expect } from 'chai'
import configureMockStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import App from '../src/App'
import { Inflist } from '../src/Inflist'
import rootReducer from '../src/reducers'
import { powerOn, itemAdd } from '../src/actions'
import { createSandbox, fake } from 'sinon'

describe('(Component) App', function () {
  const mockStore = configureMockStore([thunkMiddleware])
  let store, wrapper, sinon

  def('initial' /* state */, () => ({}))

  beforeEach(() => {
    sinon = sinon || createSandbox()
    sinon.restore()

    store = mockStore($initial)
    wrapper = mount(<Provider store={store}><App /></Provider>)
  })

  describe('powered off', function () {
    it('renders', () => {
      expect(wrapper).to.have.length(1)
    })

    it('should dispatch powerOn on click', () => {
      expect(store.getActions()).to.deep.equal([])
      wrapper.find('a.power').simulate('click')
      expect(store.getActions()).to.deep.equal([powerOn()])
    })

    it('shouldn\'t have a loading icon', () => {
      expect(wrapper.find('.loading').length).to.equal(0)
    })
  })

  describe('powered on without items', function () {
    def('initial', () => ({power: true, items: []}))

    it('should have a loading icon', () => {
      expect(wrapper.find('.loading').length).to.equal(1)
    })
  })

  describe('powered on with items', function () {
    def('initial', () => {
      sinon.replace(Inflist.prototype, 'shouldLoadMoreContent', fake.returns(true))
      return {power: true, items: [1]}
    })

    it('shouldn\'t have a loading icon', () => {
      expect(wrapper.find('.loading').length).to.equal(0)
    })

    it('should load more content') /*, () => {
      // this fails and i'm not sure how to mock it corretly
      expect(store.getActions()).not.to.deep.equal([])
    }) */
  })
})

describe('(Component) App integration', function () {
  let store, wrapper

  beforeEach(() => {
    store = createStore(rootReducer, {}, applyMiddleware(thunkMiddleware))
    wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    )
  })

  it('should remove power button on powerOn', () => {
    wrapper.find('a.power').simulate('click')
    expect(wrapper.find('App').props().power).to.equal(true)
    expect(wrapper.find('App > div').hasClass('on')).to.equal(true)
  })
})
